/*!
# Peabody  
Learning the language required to speak.
*/

mod prelude;
pub(crate) use self::prelude::*;

fn main() {
    println!("Hello, world!");
}
